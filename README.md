Generic Call For Speakers
---

## Dependencies

- Ruby 2.3.0
- Rails 4.2.5
- PostgreSQL 9.4.5

## Install

```bash
$ git clone git@bitbucket.org:garulab/genericcfs.git
$ cd genericcfs/
$ bundle
```

Copy the sample config file and update it with your database credentials (```username``` e ```password```)

```bash
$ cp config/database.yml.sample config/database.yml
```
### Setting the databas

```bash
$ rake db:setup
```

This command will create the database, run its migrations and seed it with some data for test.

## How to use
The system has three types of users:

`Admin` - Can do any operation (`CRUD`) in an entity.

- Email: `admin@mail.com`
- Password: `12345678`

`Participant` - Can see events and other participants, vote, create event and apply for events. Can only update and destroy its own data.

- Email: `participant_a@mail.com`
- Password: `12345678`

- Email: `participant_b@mail.com`
- Password: `12345678`

- Email: `participant_c@mail.com`
- Password: `12345678`

`Guest` - Can see participants and events.

- Email: `guest@mail.com`
- Password: `12345678`

The main idea is that you can create an event and people can apply to be the speakers,
the public vote for whom they want to see speaking at the event and that's how the speakers are selected.
I will show a basic workflow of how to use the app:

As ParticipantA, I can create a new event.

- `Endpoint`: POST `/v1/events`
- `Payload`: `'{ "name": "My Event", "end_date": "Tue, 15 Mar 2016 00:26:02 UTC +00:00"  }'`
- curl:

```bash
$ curl -XPOST -H "Content-Type: application/json" http://localhost:3000/v1/events  --user participant_a@mail.com:12345678 -d '{ "name": "My Event", "end_date": "Tue, 15 Mar 2016 00:26:02 UTC +00:00"  }'
```
- Reponse:

```json
{
    "id":2,
    "name":"My Event",
    "end_date":"2016-03-15T00:26:02.000Z",
    "created_at":"2016-03-08T00:46:05.863Z",
}
```

Now, ParticipantB can apply to the event to be voted for:

- `Endpoint`: POST `/v1/events/:event_id/apply`
- `Payload`: `Nothing`
- curl:

```bash
$ curl -XPOST -H "Content-Type: application/json" http://localhost:3000/v1/events/2/apply --user participant_b@mail.com:12345678 -d ''
```
- Reponse:

```json
{
  "id":2,
    "name":"My Event",
    "total_of_votes":0,
    "participants": [{
      "id": 3,
      "name":"ParticipantB",
      "votes":0,
      "url":http://localhost:3000/v1/users/3",
      "vote_url":"http://localhost:3000/v1/events/2/votes"
    }
  ]
}
```

And now, ParticipantC, can vote in the participants that he wants in the event. He can use the `vote_url` for this:

- `Endpoint`: POST `/v1/events/:event_id/votes`
- `Payload`: `'{ "user_id": "3" }'` # to voto in the ParticipantB
- curl:

```bash
$ curl -XPOST -H "Content-Type: application/json" http://localhost:3000/v1/events/2/votes --user participant_c@mail.com:12345678 -d '{ "user_id": "3" }'
```
- Reponse:

```json
{
  "id":2,
    "event": {
      "name":"My Event",
      "url":"http://localhost:3000/v1/events/2"
    },
    "participant": {
      "name":"ParticipantB",
      "delete_vote":"http://localhost:3000/v1/events/2/votes/2"
    }
}
```

And ParticipantC can delete his vote using `delete_vote` url:

- `Endpoint`: DELETE `/v1/events/:event_id/votes/:votes_id`
- `Payload`: `Nothing`
- curl:

```bash
$ curl -XDELETE -H "Content-Type: application/json" http://localhost:3000/v1/events/2/votes/2 --user participant_c@mail.com:12345678
```
- Reponse: `204 No Content`

## how to test

Rake is already set with a default task which runs the test, just run:

```bash
$ rake
```
 or

```bash
$ rspec
```

The tests are in the `specs` folder and it's possible to run them one by one.

```bash
$ rspec /path/to/file_spec.tb
```

## TODO

- Improve error handling
- Improve authorizations
- Improve authentication (using a token strategy with refresh token, maybe)
- Handle edge cases
- Add a better HATEOAS support
- Add more specs (edge cases, for example)

Rails.application.routes.draw do
  namespace :v1, defaults: { format: :json } do
    resources :users, except: [:new, :edit]
    resources :events, except: [:new, :edit] do
      post :apply, action: :apply
      resources :votes, only: [:create, :destroy]
    end
  end
end

class Vote < ActiveRecord::Base
  belongs_to :owner, class_name: 'User'
  belongs_to :user
  belongs_to :event

  validates_presence_of :owner_id, :user_id, :event_id
end

class Event < ActiveRecord::Base
  belongs_to :owner, class_name: 'User'
  has_many :votes
  has_and_belongs_to_many :users

  validates_presence_of :name, :end_date, :owner_id
  validates :finished, inclusion: { in: [true, false] }

  # Custom Validations
  validate :date_in_the_past

  def total_of_votes
    votes.count
  end

  private

  def date_in_the_past
    errors.add(:end_date, 'must be more than current date') if end_date.nil? || end_date <= Time.current
  end
end

class User < ActiveRecord::Base
  devise :database_authenticatable, :trackable, :validatable
  enum role: [:admin, :participant, :guest]

  has_many :choices, class_name: 'Vote', foreign_key: :owner_id
  has_many :events, foreign_key: :owner_id
  has_many :votes
  has_and_belongs_to_many :events

  validates_presence_of :name, :role

  def votes_by_event(event_id)
    votes.where(event_id: event_id)
  end
end

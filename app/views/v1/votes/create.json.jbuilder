json.(@vote, :id)

event = @vote.event

json.event do
  json.name event.name
  json.url v1_event_url(@vote.event)
end

json.participant do
  json.name @vote.user.name
  json.delete_vote v1_event_vote_url(event, @vote)
end

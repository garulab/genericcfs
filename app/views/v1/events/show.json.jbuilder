json.(@event, :id, :name, :total_of_votes)

json.participants @event.users do |participant|
  json.id participant.id
  json.name participant.name
  json.votes participant.votes_by_event(@event.id).count
  json.url v1_user_url(participant)
  json.vote_url v1_event_votes_url(@event)
end

class EventPolicy < ApplicationPolicy
  def create?
    user.admin? || user.participant?
  end

  def update?
    return if user.guest?
    user.admin? || record.owner_id == user.id
  end

  def apply?
    return if user.guest?
    user.admin? || user.participant?
  end

  def destroy?
    return if user.guest?
    user.admin? || record.owner_id == user.id
  end
end

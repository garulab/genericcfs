class UserPolicy < ApplicationPolicy
  def create?
    user.admin?
  end

  def update?
    return if user.guest?
    user.admin? || record.id == user.id
  end

  def destroy?
    user.admin?
  end
end

module Authentication
  private

  def authenticate
    authenticate_or_request_with_http_basic do |email, password|
      user = User.find_by(email: email)
      return head :unauthorized unless user
      if user.valid_password?(password)
        @current_user = user
      end
    end
  end
end

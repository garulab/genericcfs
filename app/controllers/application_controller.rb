require 'application_responder'

class ApplicationController < ActionController::Base
  include Authentication
  include ApplicationHelper
  include Pundit
  self.responder = ApplicationResponder
  respond_to :json
  before_filter :authenticate

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    head :unauthorized
  end
end

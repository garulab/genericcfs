module V1
  class VotesController < ApplicationController
    def create
      @vote = current_user.choices.build(vote_params)
      @vote.event_id = params[:event_id]
      authorize(@vote)

      @vote.save
      respond_with(@vote)
    end

    def destroy
      @vote = Vote.find(params[:id])
      authorize(@vote)

      @vote.destroy
      respond_with(@vote)
    end

    private

    def vote_params
      params.require(:vote).permit(:user_id)
    end
  end
end

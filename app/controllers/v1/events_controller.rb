module V1
  class EventsController < ApplicationController
    before_action :set_event, only: [:show, :update, :destroy]

    def index
      @events = Event.all
      respond_with(@events)
    end

    def show
      respond_with(@event)
    end

    def create
      @event = Event.new(event_params)
      @event.owner = current_user
      authorize(@event)

      @event.save
      respond_with(
        @event,
        location: -> { v1_event_path(@event) },
        status: 201
      )
    end

    def update
      authorize(@event)

      @event.update(event_params)
      respond_with(
        @event,
        json: @event,
        location: -> { v1_event_path(@event) }
      )
    end

    def apply
      @event = Event.find(params[:event_id])
      authorize(@event)

      @event.users << current_user
      @event.save
      respond_with(
        @event,
        location: -> { v1_event_path(@event) },
      )
    end

    def destroy
      authorize(@event)

      @event.destroy
      respond_with(nothing: true)
    end

    private

    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:name, :end_date)
    end
  end
end

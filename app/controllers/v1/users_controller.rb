module V1
  class UsersController < ApplicationController
    before_action :set_user, only: [:show, :update, :destroy]

    def index
      @users = User.all
      respond_with(@users)
    end

    def show
      respond_with(@user)
    end

    def create
      @user = User.new(user_params)
      authorize(@user)

      @user.save
      respond_with(@user, location: -> { v1_user_path(@user) })
    end

    def update
      authorize(@user)

      @user.update(user_params)
      respond_with(
        @user,
        json: @user,
        location: -> { v1_event_path(@user) }
      )
    end

    def destroy
      authorize(@user)

      @user.destroy
      respond_with(@user)
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :password)
    end
  end
end

require 'rails_helper'

RSpec.describe Vote, type: :model do
  it { is_expected.to validate_presence_of(:owner_id) }
  it { is_expected.to validate_presence_of(:user_id) }
  it { is_expected.to validate_presence_of(:event_id) }
end

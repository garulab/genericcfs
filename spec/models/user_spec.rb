require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:role) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:password) }

  it 'already has the same email registered' do
    create(:user, email: 'example@example.com')

    user = build(:user, email: 'example@example.com')
    expect(user).to be_invalid
  end

  describe 'Relationships' do
    describe '#events' do
      it 'must return an empty collection if has no event' do
        user = build(:user)

        expect(user.events).to eq([])
      end

      it 'must return the user\'s events' do
        user = build(:user)
        events = build_list(:event, 2)

        user.events << events
        user.save!

        expect(user.events).to match_array(events)
      end

      it 'must change the events count' do
        user = build(:user)
        event = build(:event)

        user.events << event

        expect { user.save! }.to change { user.events.count }.by(1)
      end
    end

    describe '#votes_by_event' do
      it 'must return the votes the user received in specific event' do
        participant = build(:user)
        event = build(:event)

        event.users << participant
        event.save!
        create(:vote, user: participant, event: event)

        expect(participant.votes_by_event(event.id).count).to eq(1)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Event, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:end_date) }
  it { is_expected.to validate_presence_of(:owner_id) }

  context 'is valid when' do
    specify 'finished is not nil' do
      event = build(:event, finished: false)

      expect(event).to be_valid
    end
  end

  context 'is invalid when' do
    specify 'finished is nil' do
      event = build(:event, finished: nil)

      expect(event).to be_invalid
    end

    specify 'end date is before or equal current date' do
      event = build(:event, end_date: -1.days.from_now)

      expect(event).to be_invalid
    end
  end

  describe 'Relationships' do
    describe '#users' do
      it 'cannot have the same user twice' do
        event = build(:event)
        user = build(:user)

        event.users << user
        event.users << user

        expect { event.save! }.to raise_error(ActiveRecord::RecordNotUnique)
      end
    end

    describe '#total_of_votes' do
      it 'must return total of votes' do
        event = create(:event)

        create(:vote, event: event)

        expect(event.total_of_votes).to eq(1)
      end

      it 'must return only votes from the event' do
        event = create(:event)
        other_event = create(:event)

        create(:vote, event: event)
        create(:vote, event: other_event)

        expect(event.total_of_votes).to eq(1)
      end
    end
  end
end

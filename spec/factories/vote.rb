FactoryGirl.define do
  factory :vote do
    association :owner, factory: :user
    association :user
    association :event
  end
end

FactoryGirl.define do
  factory :event do
    sequence(:name) { |i| "Event #{i}" }
    end_date 7.days.from_now
    finished false

    association :owner, factory: :user
  end
end

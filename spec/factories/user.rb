FactoryGirl.define do
  factory :user do
    sequence(:name) { |i| "User #{i}" }
    sequence(:email) { |i| "participant#{i}@mail.com" }
    password '12345678'
    role :participant

    trait :admin do
      sequence(:name) { |i| "Admin #{i}" }
      sequence(:email) { |i| "admin#{i}@mail.com" }
      role :admin
    end

    trait :guest do
      sequence(:name) { |i| "Guest #{i}" }
      sequence(:email) { |i| "guest#{i}@mail.com" }
      role :guest
    end
  end
end

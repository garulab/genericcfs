require 'rails_helper'
require 'pundit/rspec'

RSpec.describe UserPolicy do
  subject(:user_policy) { described_class }

  permissions :create? do
    it 'grants access for admin user' do
      expect(user_policy).to permit(
        User.new(role: :admin), User.new
      )
    end

    it 'denies access for participant user' do
      expect(user_policy).not_to permit(
        User.new(role: :participant), User.new
      )
    end

    it 'denies access for guest user' do
      expect(user_policy).not_to permit(
        User.new(role: :guest), User.new
      )
    end
  end

  permissions :update? do
    it 'grants access for admin user' do
      expect(user_policy).to permit(
        User.new(role: :admin), User.new
      )
    end

    it "grants access for participant user if it's its informations" do
      expect(user_policy).to permit(
        User.new(role: :participant, id: 1), User.new(id: 1)
      )
    end

    it 'denies access for guest user' do
      expect(user_policy).not_to permit(
        User.new(role: :guest), User.new
      )
    end
  end

  permissions :destroy? do
    it 'grants access for admin user' do
      expect(user_policy).to permit(
        User.new(role: :admin), User.new
      )
    end

    it 'denies access for participant user' do
      expect(user_policy).not_to permit(
        User.new(role: :participant), User.new
      )
    end

    it 'denies access for guest user' do
      expect(user_policy).not_to permit(
        User.new(role: :guest), User.new
      )
    end
  end
end

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe EventPolicy do
  subject(:event_policy) { described_class }

  permissions :create? do
    it 'grants access for admin user' do
      expect(event_policy).to permit(
        User.new(role: :admin), Event.new
      )
    end

    it 'grants access for participant user' do
      expect(event_policy).to permit(
        User.new(role: :participant), Event.new
      )
    end

    it 'denies access for guest user' do
      expect(event_policy).not_to permit(
        User.new(role: :guest), Event.new
      )
    end
  end

  permissions :update? do
    it 'grants access for admin user' do
      expect(event_policy).to permit(
        User.new(role: :admin), Event.new
      )
    end

    it "grants access for participant user if it's the event owner" do
      expect(event_policy).to permit(
        User.new(role: :participant, id: 1 ), Event.new(owner_id: 1)
      )
    end

    it 'denies access for guest user' do
      expect(event_policy).not_to permit(
        User.new(role: :guest), Event.new
      )
    end
  end

  permissions :apply? do
    it 'grants access for admin user' do
      expect(event_policy).to permit(
        User.new(role: :admin), Event.new
      )
    end

    it "grants access for participant user" do
      expect(event_policy).to permit(
        User.new(role: :participant), Event.new
      )
    end

    it 'denies access for guest user' do
      expect(event_policy).not_to permit(
        User.new(role: :guest), Event.new
      )
    end
  end

  permissions :destroy? do
    it 'grants access for admin user' do
      expect(event_policy).to permit(
        User.new(role: :admin), Event.new
      )
    end

    it "grants access for participant user if it's the event owner" do
      expect(event_policy).to permit(
        User.new(role: :participant, id: 1), Event.new(owner_id: 1 )
      )
    end

    it 'denies access for guest user' do
      expect(event_policy).not_to permit(
        User.new(role: :guest), Event.new
      )
    end
  end
end

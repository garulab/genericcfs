require 'rails_helper'
require 'pundit/rspec'

RSpec.describe VotePolicy do
  subject(:vote_policy) { described_class }

  permissions :create? do
    it 'grants access for admin user' do
      expect(vote_policy).to permit(
        User.new(role: :admin), Vote.new
      )
    end

    it 'grants access for participant user' do
      expect(vote_policy).to permit(
        User.new(role: :participant), Vote.new
      )
    end

    it 'denies access for guest user' do
      expect(vote_policy).not_to permit(
        User.new(role: :guest), Vote.new
      )
    end
  end

  permissions :destroy? do
    it 'grants access for admin user' do
      expect(vote_policy).to permit(
        User.new(role: :admin), Vote.new
      )
    end

    it "grants access for participant user if it's the vote owner" do
      expect(vote_policy).to permit(
        User.new(role: :participant, id: 1), Vote.new(owner_id: 1)
      )
    end

    it 'denies access for guest user' do
      expect(vote_policy).not_to permit(
        User.new(role: :guest), Vote.new
      )
    end
  end
end

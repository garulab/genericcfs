require 'rails_helper'

RSpec.describe V1::VotesController, type: :controller do
  render_views

  let(:participant) { create(:user) }
  let(:event) { create(:event) }

  let(:valid_attributes) do
    { user_id: participant.id }
  end

  let(:invalid_attributes) do
    { user_id: nil }
  end

  let(:new_attributes) { { name: 'New name' } }

  def post_create(attributes)
    post :create, event_id: event.id, vote: attributes, format: :json
  end

  context 'Admin user' do
    let(:admin) { create(:user, :admin) }

    before do
      basic_auth_sign_in(admin)
    end

    describe 'POST #create' do
      context 'with valid params' do
        it 'vote in a participant' do
          expect {
            post_create(valid_attributes)
          }.to change(Vote, :count).by(1)
        end

        it 'respond with newly created vote' do
          post_create(valid_attributes)

          vote_response = json_response
          expect(vote_response).to have_key(:id)
        end

        it 'respond with event informations' do
          post_create(valid_attributes)

          vote_response = json_response
          expect(vote_response).to have_key(:event)
          expect(vote_response[:event][:name]).to eq(event.name)
          expect(vote_response[:event][:url]).to eq(v1_event_url(event))
        end

        it 'respond with participant informations' do
          post_create(valid_attributes)

          vote_response = json_response
          expect(vote_response).to have_key(:participant)
          expect(vote_response[:participant][:name]).to eq(participant.name)
          expect(vote_response[:participant][:delete_vote]).to eq(
            v1_event_vote_url(event, vote_response[:id])
          )
        end
      end

      context 'with invalid params' do
        it 'respond with unprocessable entity' do
          post_create(invalid_attributes)

          expect(response.status).to eq(422)
        end

        it 'respond with errors' do
          post_create(invalid_attributes)

          errors_response = json_response
          expect(errors_response).to have_key(:errors)
          expect(errors_response[:errors][:user_id][0]).to eq(
            t('activerecord.errors.messages.blank')
          )
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'deletes the requested vote' do
        vote = create(:vote)

        expect {
          delete :destroy, event_id: event.id, id: vote.id, format: :json
        }.to change(Vote, :count).by(-1)
      end

      it 'respond with no content' do
        vote = create(:vote)

        delete :destroy, event_id: event.id, id: vote.id, format: :json

        expect(response.status).to eq(204)
      end
    end
  end

  context 'Participant user' do
    let(:user) { create(:user) }

    before do
      basic_auth_sign_in(user)
    end

    it 'can vote in a participant' do
      post_create(valid_attributes)
      expect(response.status).to eq(200)
    end

    it 'cannot delete a vote that is not its own' do
      other_user = create(:user)
      vote = create(:vote, owner: other_user, event: event, user: participant)

      delete :destroy, event_id: event.id, id: vote.id, format: :json

      expect(response.status).to eq(401)
    end
  end

  context 'Guest user' do
    let(:guest) { create(:user, :guest) }

    before do
      basic_auth_sign_in(guest)
    end

    it 'cannot vote' do
      post_create(valid_attributes)
      expect(response.status).to eq(401)
    end

    it 'cannot delete a vote' do
      user = create(:user)
      vote = create(:vote, owner: user, event: event, user: participant)

      delete :destroy, event_id: event.id, id: vote.id, format: :json

      expect(response.status).to eq(401)
    end
  end
end

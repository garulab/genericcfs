require 'rails_helper'

RSpec.describe V1::UsersController, type: :controller do
  let(:user) { create(:user) }
  let(:admin) { create(:user, :admin) }

  let(:valid_attributes) do
    { name: 'New user', email: 'new@user.com', password: '12345678' }
  end

  let(:invalid_attributes) do
    { name: nil, email: nil, password: nil }
  end

  let(:new_attributes) { { email: 'new@email.com' } }

  def post_create(attributes)
    post :create, user: attributes, format: :json
  end

  def put_update(attributes)
    post :update, id: user.id, user: attributes, format: :json
  end

  context 'Admin user' do
    before do
      basic_auth_sign_in(admin)
    end

    describe 'GET #index' do
      it 'respond with all users' do
        get :index, format: :json

        users_response = json_response
        expect(users_response[0][:email]).to eq(admin.email)
      end
    end

    describe 'GET #show' do
      it 'respond with the requested user' do
        get :show, id: user.id, format: :json

        user_response = json_response
        expect(user_response[:email]).to eq(user.email)
      end
    end

    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new user' do
          expect {
            post_create(valid_attributes)
          }.to change(User, :count).by(1)
        end

        it 'respond with newly created user' do
          post_create(valid_attributes)

          user_response = json_response
          expect(user_response[:email]).to eq(valid_attributes[:email])
          expect(response.status).to eq(201)
        end
      end

      context 'with invalid params' do
        it 'respond with unprocessable entity' do
          post_create(invalid_attributes)

          expect(response.status).to eq(422)
        end

        it 'respond with errors' do
          post_create(invalid_attributes)

          errors_response = json_response
          expect(errors_response).to have_key(:errors)
          expect(errors_response[:errors][:email][0]).to eq(
            t('activerecord.errors.messages.blank')
          )
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        it 'respond with the requested user updated' do
          put_update(new_attributes)

          user_response = json_response
          expect(user_response[:email]).to eq(new_attributes[:email])
        end

        it 'respond with status ok' do
          put_update(new_attributes)

          expect(response.status).to eq(200)
        end

        it 'respond with location header' do
          put_update(new_attributes)

          user_response = json_response
          expect(response.location).to eq(v1_event_path(user_response[:id]))
        end
      end

      context 'with invalid params' do
        it 'respond with unprocessable entity' do
          put_update(invalid_attributes)

          expect(response.status).to eq(422)
        end

        it 'respond with errors' do
          put_update(invalid_attributes)

          errors_response = json_response
          expect(errors_response).to have_key(:errors)
          expect(errors_response[:errors][:email][0]).to eq(
            t('activerecord.errors.messages.blank')
          )
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested user' do
        user = create(:user)

        expect {
          delete :destroy, id: user.id, format: :json
        }.to change(User, :count).by(-1)
      end

      it 'respond with no content' do
        user = create(:user)

        delete :destroy, id: user.id, format: :json

        expect(response.status).to eq(204)
      end
    end
  end

  context 'Participant user' do
    before do
      basic_auth_sign_in(user)
    end

    it 'can see all users' do
      get :index, format: :json

      expect(response.status).to eq(200)
    end

    it 'can see a specific user' do
      other_user = create(:user)

      get :show, id: other_user.id, format: :json

      expect(response.status).to eq(200)
    end

    it 'cannot update information of other user' do
      put :update, id: admin.id, user: new_attributes, format: :json

      expect(response.status).to eq(401)
    end

    it 'cannot create a new user' do
      post_create(valid_attributes)

      expect(response.status).to eq(401)
    end

    it 'cannot destroy a user' do
      other_user = create(:user)

      delete :destroy, id: other_user.id, format: :json

      expect(response.status).to eq(401)
    end
  end

  context 'Guest user' do
    let(:guest) { create(:user, :guest) }

    before do
      basic_auth_sign_in(guest)
    end

    it 'can see all users' do
      get :index, format: :json

      expect(response.status).to eq(200)
    end

    it 'can see a specific user' do
      other_user = create(:user)

      get :show, id: other_user.id, format: :json

      expect(response.status).to eq(200)
    end

    it 'cannot update any information' do
      put :update, id: guest.id, user: new_attributes, format: :json

      expect(response.status).to eq(401)
    end

    it 'cannot create a new user' do
      post_create(valid_attributes)

      expect(response.status).to eq(401)
    end

    it 'cannot destroy a user' do
      other_user = create(:user)

      delete :destroy, id: other_user.id, format: :json

      expect(response.status).to eq(401)
    end
  end
end

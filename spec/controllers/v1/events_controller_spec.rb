require 'rails_helper'

RSpec.describe V1::EventsController, type: :controller do
  render_views

  let(:user) { create(:user) }
  let(:event) { create(:event) }

  let(:valid_attributes) do
    { name: 'New Event', end_date: 7.days.from_now }
  end

  let(:invalid_attributes) do
    { name: nil, end_date: 7.days.from_now }
  end

  let(:new_attributes) { { name: 'New name' } }

  def post_create(attributes)
    post :create, event: attributes, format: :json
  end

  def put_update(attributes)
    put :update, id: event.id, event: attributes, format: :json
  end

  context 'Admin user' do
    let(:admin) { create(:user, :admin) }

    before do
      basic_auth_sign_in(admin)
    end

    describe 'GET #index' do
      it 'respond with all events' do
        event = create(:event)

        get :index, format: :json

        events_response = json_response
        expect(events_response[0][:name]).to eq(event.name)
      end
    end

    describe 'GET #show' do
      it 'respond with the requested event' do
        get :show, id: event.id, format: :json

        event_response = json_response
        expect(event_response[:name]).to eq(event.name)
      end

      it 'respond with event\'s total of votes' do
        participant = create(:user)
        event = create(:event, users: [participant])
        create(:vote, owner: admin, event: event, user: participant)

        get :show, id: event.id, format: :json

        event_response = json_response
        expect(event_response[:total_of_votes]).to eq(1)
      end

      it 'respond with all event\'s participants' do
        participant = create(:user)
        event = create(:event, users: [participant])

        get :show, id: event.id, format: :json

        event_response = json_response
        expect(event_response[:participants][0][:name]).to eq(participant.name)
        expect(event_response[:participants][0][:url]).to eq(
          v1_user_url(participant)
        )
        expect(event_response[:participants][0][:vote_url]).to eq(
          v1_event_votes_url(event)
        )
      end

      it 'respond with participants\'s votes' do
        participant = create(:user)
        event = create(:event, users: [participant])
        create(:vote, owner: admin, event: event, user: participant)

        get :show, id: event.id, format: :json

        event_response = json_response
        expect(event_response[:participants][0][:votes]).to eq(1)
      end
    end

    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new event' do
          expect {
            post_create(valid_attributes)
          }.to change(Event, :count).by(1)
        end

        it 'respond with newly created event' do
          post_create(valid_attributes)

          event_response = json_response
          expect(event_response[:email]).to eq(valid_attributes[:email])
          expect(response.status).to eq(201)
        end

        it 'respond without unnecessary informations' do
          post_create(valid_attributes)

          event_response = json_response
          expect(event_response).not_to have_key(:owner_id)
          expect(event_response).not_to have_key(:finished)
          expect(event_response).not_to have_key(:updated_at)
        end

        it 'respond with location header' do
          post_create(valid_attributes)

          event_response = json_response
          expect(response.location).to eq(v1_event_path(event_response[:id]))
        end
      end

      context 'with invalid params' do
        it 'respond with unprocessable entity' do
          post_create(invalid_attributes)

          expect(response.status).to eq(422)
        end

        it 'respond with errors' do
          post_create(invalid_attributes)

          errors_response = json_response
          expect(errors_response).to have_key(:errors)
          expect(errors_response[:errors][:name][0]).to eq(
            t('activerecord.errors.messages.blank')
          )
        end
      end
    end

    describe 'POST #apply' do
      it 'respond with a user that apply for an event' do
        post :apply, event_id: event.id, format: :json

        event_response = json_response
        expect(event_response[:participants][0][:name]).to eq(admin.name)
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        it 'respond with the requested event updated' do
          put_update(new_attributes)

          event_response = json_response
          expect(event_response[:name]).to eq(new_attributes[:name])
        end

        it 'respond with status ok' do
          put_update(new_attributes)

          expect(response.status).to eq(200)
        end

        it 'respond with location header' do
          put_update(new_attributes)

          event_response = json_response
          expect(response.location).to eq(v1_event_path(event_response[:id]))
        end
      end

      context 'with invalid params' do
        it 'respond with unprocessable entity' do
          put_update(invalid_attributes)

          expect(response.status).to eq(422)
        end

        it 'respond with errors' do
          put_update(invalid_attributes)

          errors_response = json_response
          expect(errors_response).to have_key(:errors)
          expect(errors_response[:errors][:name][0]).to eq(
            t('activerecord.errors.messages.blank')
          )
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested event' do
        event = create(:event)

        expect {
          delete :destroy, id: event.id, format: :json
        }.to change(Event, :count).by(-1)
      end

      it 'respond with no content' do
        event = create(:event)

        delete :destroy, id: event.id, format: :json

        expect(response.status).to eq(204)
      end
    end
  end

  context 'Participant user' do
    before do
      basic_auth_sign_in(user)
    end

    it 'can see all events' do
      get :index, format: :json

      expect(response.status).to eq(200)
    end

    it 'can see a specific event' do
      event = create(:event)

      get :show, id: event.id, format: :json

      expect(response.status).to eq(200)
    end

    it 'cannot update event informations that it is not its own' do
      put_update(new_attributes)

      expect(response.status).to eq(401)
    end

    it 'can create a event' do
      post_create(valid_attributes)

      expect(response.status).to eq(201)
    end

    it 'cannot destroy a event that is not its own' do
      other_user = create(:user)
      event = create(:event, owner: other_user)

      delete :destroy, id: event.id, format: :json

      expect(response.status).to eq(401)
    end
  end

  context 'Guest user' do
    let(:guest) { create(:user, :guest) }

    before do
      basic_auth_sign_in(guest)
    end

    it 'can see all events' do
      get :index, format: :json

      expect(response.status).to eq(200)
    end

    it 'can see a specific event' do
      get :show, id: event.id, format: :json

      expect(response.status).to eq(200)
    end

    it 'cannot update any information' do
      put_update(new_attributes)

      expect(response.status).to eq(401)
    end

    it 'cannot create a new event' do
      post_create(valid_attributes)

      expect(response.status).to eq(401)
    end

    it 'cannot destroy a event' do
      delete :destroy, id: event.id, format: :json

      expect(response.status).to eq(401)
    end
  end
end

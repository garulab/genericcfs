require 'rails_helper'

RSpec.describe V1::EventsController, type: :controller do
  controller do
    def index
    end
  end

  describe 'login with inexitent user' do
    it 'must respond with access denied' do
      basic_auth_sign_in(build_stubbed(:user))

      get :index

      expect(response.status).to eq(401)
    end
  end
end

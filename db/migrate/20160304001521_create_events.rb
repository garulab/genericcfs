class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.datetime :end_date, null: false
      t.boolean :finished, null: false, default: false
      t.references :owner, references: :users, null: false, index: true

      t.timestamps null: false
    end
    add_foreign_key :events, :users, column: :owner_id, on_delete: :cascade
  end
end

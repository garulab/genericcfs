class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :owner, references: :users, null: false, index: true
      t.references :user, index: true, null: false
      t.references :event, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :votes, :users, column: :owner_id, on_delete: :cascade
    add_foreign_key :votes, :users, on_delete: :cascade
    add_foreign_key :votes, :events, on_delete: :cascade
  end
end

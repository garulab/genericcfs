require 'factory_girl_rails'

puts "###############################################"

if User.count.zero?
  puts "\n## Creating admin user ##"
  admin = FactoryGirl.create(:user, :admin, name: 'Admin', email: 'admin@mail.com')
  puts "Name: #{admin.name} \nEmail: #{admin.email} \nSenha: 12345678"
  puts "\n.............................."

  puts "\n## Creating participant users ##"
  %w{ParticipantA ParticipantB ParticipantC}.each do |p|
    FactoryGirl.create(:user, name: p, email: "#{p.underscore}@mail.com")
    puts "\nName: #{p} \nEmail: #{p.underscore}@mail.com \nSenha: 12345678"
  end

  puts "\n.............................."

  puts "\n## Creating guest user ##"
  guest = FactoryGirl.create(:user, :guest)
  puts "Name: #{guest.name} \nEmail: #{guest.email} \nSenha: 12345678"
end

puts "\n.............................."

if Event.count.zero?
  admin ||= User.where(role: 'admin').first
  puts "\n## Creating Event ##"
  FactoryGirl.create(:event, owner_id: admin.id)
  puts "Event 001 created \nEnd date: #{2.weeks.from_now}"
end

puts "\n.............................."

if Vote.count.zero?
  admin ||= User.where(role: 0).first
  event ||= Event.first
  speaker = User.where(role: 1).first
  event.users << speaker
  event.save!

  puts "\n## Creating Votes ##"
  Vote.create(owner_id: admin.id, user_id: speaker.id, event_id: event.id)
  puts "Admin voted on #{speaker.name} in #{event.name}"
end

puts "\n###############################################"
